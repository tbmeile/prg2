package ch.h16.sw3.apocalypse.model;

import ch.h16.sw3.apocalypse.control.PopulationGenerator;
import ch.h16.sw3.apocalypse.view.MultiView;
import ch.h16.sw3.apocalypse.view.SimulatorView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A class representing shared characteristics of animals.
 *
 * @author David J. Barnes and Michael Kolling
 * @version 2008.03.30
 */
public abstract class Animal implements Actor {

    // Whether the animal is alive or not.
    private boolean alive;
    // The animal's field.
    private Field field;
    // The animal's position in the field.
    private Location location;

    /**
     * Create a new animal at location in field.
     *
     * @param field The field currently occupied.
     * @param location The location within the field.
     */
    public Animal(Field field, Location location) {
        alive = true;
        this.field = field;
        setLocation(location);
    }

    /**
     * Make this animal act - that is: make it do whatever it wants/needs to do.
     *
     * @param newAnimals A list to add newly born animals to.
     */
    @Override
    abstract public void act(List<Actor> newAnimals);

    /**
     * Check whether the animal is alive or not.
     *
     * @return true if the animal is still alive.
     */
    @Override
    public boolean isActive() {
        return alive;
    }

    /**
     * Indicate that the animal is no longer alive. It is removed from the
     * field.
     */
    public void setDead() {
        alive = false;
        if (location != null) {
            field.clear(location);
            location = null;
            field = null;
        }
    }

    /**
     * Return the animal's location.
     *
     * @return The animal's location.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Return the animal's field.
     *
     * @return The animal's field.
     */
    public Field getField() {
        return field;
    }

    /**
     * Place the animal at the new location in the given field.
     *
     * @param newLocation The animal's new location.
     */
    public void setLocation(Location newLocation) {
        if (location != null) {
            field.clear(location);
        }
        location = newLocation;
        field.place(this, newLocation);
    }

    abstract protected int breed();

    /**
     * Check whether or not this animal is to give birth at this step. New
     * births will be made into free adjacent locations.
     *
     * @param newborn A list to add newly born animals to.
     */
    protected void giveBirth(List<Actor> newborn) {
        // New rabbits are born into adjacent locations.
        // Get a list of adjacent free locations.
        Field field = getField();
        List<Location> free = field.getFreeAdjacentLocations(getLocation());
        int births = breed();
        for (int b = 0; b < births && free.size() > 0; b++) {
            Location loc = free.remove(0);
            newborn.add(createAnimal(false, field, loc));
        }
    }

    /**
     * Create a new animal. An animal may be created with age zero (a new born)
     * or with a random age.
     *
     * @param randomAge If true, the animal will have a random age.
     * @param field The field currently occupied.
     * @param location The location within the field.
     */
    abstract protected Animal createAnimal(boolean randomAge, Field field, Location location);

    /**
     * A simple predator-prey simulator, based on a rectangular field containing
     * rabbits and foxes.
     *
     * @author David J. Barnes and Michael Kolling
     * @version 2008.03.30
     */
    public static class Simulator {

        // Constants representing configuration information for the simulation.
        // The default width for the grid.
        private static final int DEFAULT_WIDTH = 50;
        // The default depth of the grid.
        private static final int DEFAULT_DEPTH = 50;

        // List of actors in the field.
        private List<Actor> actors;
        // The current state of the field.
        private Field field;
        // The current step of the simulation.
        private int step;
        // A graphical view of the simulation.
        private SimulatorView view;
        // A population generator.
        private PopulationGenerator generator;

        /**
         * Construct a simulation field with default size.
         */
        public Simulator() {
            this(DEFAULT_DEPTH, DEFAULT_WIDTH);
        }

        /**
         * Create a simulation field with the given size.
         *
         * @param depth Depth of the field. Must be greater than zero.
         * @param width Width of the field. Must be greater than zero.
         */
        public Simulator(int depth, int width) {
            if (width <= 0 || depth <= 0) {
                System.out.println("The dimensions must be greater than zero.");
                System.out.println("Using default values.");
                depth = DEFAULT_DEPTH;
                width = DEFAULT_WIDTH;
            }

            actors = new ArrayList<Actor>();
            field = new Field(depth, width);

            // Create a view of the state of the field.
            view = new MultiView(depth, width);

            // Create a population generator.
            generator = new PopulationGenerator(view);

            // Setup a valid starting point.
            reset();
        }

        /**
         * Run the simulation from its current state for a reasonably long period,
         * e.g. 500 steps.
         */
        public void runLongSimulation() {
            simulate(500);
        }

        /**
         * Run the simulation from its current state for the given number of steps.
         * Stop before the given number of steps if it ceases to be viable.
         *
         * @param numSteps The number of steps to run for.
         */
        public void simulate(int numSteps) {
            for (int step = 1; step <= numSteps && view.isViable(field); step++) {
                simulateOneStep();
            }
        }

        /**
         * Run the simulation from its current state for a single step. Iterate over
         * the whole field updating the state of each fox and rabbit.
         */
        public void simulateOneStep() {
            step++;

            // Provide space for newborn actors.
            List<Actor> newActors = new ArrayList<Actor>();
            // Let all rabbits act.
            for (Iterator<Actor> it = actors.iterator(); it.hasNext();) {
                Actor actor = it.next();
                actor.act(newActors);
                if (!actor.isActive()) {
                    it.remove();
                }
            }

            // Add the newly born foxes and rabbits to the main lists.
            actors.addAll(newActors);

            view.showStatus(step, field);
        }

        /**
         * Reset the simulation to a starting position.
         */
        public void reset() {
            step = 0;
            actors.clear();
            field.clear();
            generator.populate(field, actors);

            // Show the starting state in the view.
            view.showStatus(step, field);
        }

    }
}
