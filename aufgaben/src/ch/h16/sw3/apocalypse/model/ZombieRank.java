package ch.h16.sw3.apocalypse.model;

/**
 * Created by alkazua on 10.03.16.
 * Project: prg2
 * Description: in progress
 */
public enum ZombieRank {
    APPRENTICE(1), NOVICE(2), SERVANT(3), MASTER(4), WARRIOR(5), CHIEFTAIN(6), KING(7), EMPEROR(8), GODLIKE(9), LEGENDARY(10);

    private static final double CONTAMINATION = 0.1;
    private static final double DEADLINESS = 0.1;
    private int factor;

    ZombieRank(int factor) {
        this.factor = factor;
    }

    public int getFactor() {
        return factor;
    }

    public double getContamination() {
        return factor*CONTAMINATION;
    }

    public double getDeadliness() {
        return factor*DEADLINESS;
    }
}
