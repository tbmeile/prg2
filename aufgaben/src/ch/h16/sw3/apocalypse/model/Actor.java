package ch.h16.sw3.apocalypse.model;

import java.util.List;

/**
 * The interface to be extended by any class wishing to participate in the
 * simulation.
 */
public interface Actor {

    /**
     * Perform the actor�s regular behavior.
     *
     * @param newActors A list for storing newly created actors.
     */
    void act(List<Actor> newActors);

    /**
     * Is the actor still active?
     *
     * @return true if still active, false if not.
     */
    boolean isActive();
}
