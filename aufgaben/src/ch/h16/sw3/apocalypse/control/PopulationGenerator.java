package ch.h16.sw3.apocalypse.control;

import ch.h16.sw3.apocalypse.model.*;
import ch.h16.sw3.apocalypse.view.SimulatorView;

import java.awt.Color;
import java.util.List;
import java.util.Random;

/**
 * A class to generate the actors of the simulation.
 *
 * @author David J. Barnes and Michael Kolling and Poul Henricksen
 * @version 2008.03.30
 */
public class PopulationGenerator {

    // The probability that a fox will be created in any given grid position.
    private static final double ZOMBIE_CREATION_PROBABILITY = 0.02;
    // The probability that a rabbit will be created in any given grid position.
    private static final double HUMAN_CREATION_PROBABILITY = 0.08;
    // The probability that a hunter will be created in any given grid position.
    private static final double HUNTER_CREATION_PROBABILITY = 0.01;

    /**
     * Constructor for objects of class PopulationGenerator
     *
     * @param view The visualization.
     */
    public PopulationGenerator(SimulatorView view) {
        // Setup associations between the animals and colors
        // for the visualization.
        view.setColor(Human.class, Color.blue);
        view.setColor(Zombie.class, Color.green);
    }

    /**
     * Randomly populate the field with actors.
     *
     * @param field The field to be populated.
     * @param actors A list of all the animals generated.
     */
    public void populate(Field field, List<Actor> actors) {
        Random rand = Randomizer.getRandom();
        for (int row = 0; row < field.getDepth(); row++) {
            for (int col = 0; col < field.getWidth(); col++) {
                if (rand.nextDouble() <= ZOMBIE_CREATION_PROBABILITY) {
                    Location location = new Location(row, col);
                    Zombie zombie = new Zombie(true, field, location);
                    actors.add(zombie);
                } else if (rand.nextDouble() <= HUMAN_CREATION_PROBABILITY) {
                    Location location = new Location(row, col);
                    Human human = new Human(true, field, location);
                    actors.add(human);
                }
                // else leave the location empty.
            }
        }
    }
}