package ch.h16.sw3.apocalypse.view;

import ch.h16.sw3.apocalypse.model.Field;
import ch.h16.sw3.apocalypse.model.FieldStats;

import java.awt.Color;

/**
 * Provide a text-output view of the simulation.
 *
 * @author David J. Barnes and Michael Kolling
 * @version 2008.03.30
 */
public class TextView implements SimulatorView {

    private final String STEP_PREFIX = "Step: ";
    private final String POPULATION_PREFIX = "Population: ";

    // A statistics object computing and storing simulation information
    private FieldStats stats;

    /**
     * Create a view of the state of the field.
     */
    public TextView() {
        stats = new FieldStats();
    }

    /**
     * Associate a class with a color.
     *
     * @param cl An actor class.
     * @param color The visualization color.
     */
    public void setColor(Class cl, Color color) {
        // No action required.
    }

    /**
     * Determine whether the field is viable or not.
     *
     * @param field The field to be examined.
     * @return true if viable, false otherwise.
     */
    public boolean isViable(Field field) {
        return stats.isViable(field);
    }

    /**
     * Show the status of the field at the given step.
     *
     * @param step The simulation step.
     * @param field The field whose status is to be shown.
     */
    public void showStatus(int step, Field field) {

        stats.reset();

        for (int row = 0; row < field.getDepth(); row++) {
            for (int col = 0; col < field.getWidth(); col++) {
                Object actor = field.getObjectAt(row, col);
                if (actor != null) {
                    stats.incrementCount(actor.getClass());
                }
            }
        }
        stats.countFinished();
        System.out.print(STEP_PREFIX + step + " ");
        System.out.println(POPULATION_PREFIX + stats.getPopulationDetails(field));
    }
}
