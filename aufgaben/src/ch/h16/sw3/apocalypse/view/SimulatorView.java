package ch.h16.sw3.apocalypse.view;

import ch.h16.sw3.apocalypse.model.Field;

import java.awt.Color;

/**
 * Write a description of interface SimulatorView here.
 *
 * @author David J. Barnes and Michael Kolling
 * @version 2008.03.30
 */
public interface SimulatorView {

    /**
     * Associate a class with a color.
     *
     * @param cl An actor class.
     * @param color The visualization color.
     */
    void setColor(Class cl, Color color);

    /**
     * Determine whether the field is viable or not.
     *
     * @param field The field to be examined.
     * @return true if viable, false otherwise.
     */
    boolean isViable(Field field);

    /**
     * Show the status of the field at the given step.
     *
     * @param step The simulation step.
     * @param field The field whose status is to be shown.
     */
    void showStatus(int step, Field field);
}
