package ch.h16.sw3.oop3;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by alkazua on 10.03.16.
 * Project: prg2
 * Description: in progress
 */
public class ListOne implements IList {
    private LinkedList<Integer> list = new LinkedList<Integer>();

    @Override
    public void add(int i) {
        list.add(i);
    }

    @Override
    public boolean remove(Integer i) {
        return list.remove(i);
    }

    @Override
    public boolean removeValue(int i) {
        return list.remove((Integer) i);
    }

    @Override
    public boolean removeValues(int i) {
        boolean modified = false;
        Iterator<Integer> e = list.iterator();
        while (e.hasNext()) {
            if (i == e.next()) {
                e.remove();
                modified = true;
            }
        }
        return modified;
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean exists() {
        return true;
    }

    @Override
    public void print() {
        for (Integer i : list) {
            System.out.println(i);
        }
    }
}
