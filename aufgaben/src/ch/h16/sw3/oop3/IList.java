package ch.h16.sw3.oop3;

/**
 * Created by alkazua on 10.03.16.
 * Project: prg2
 * Description: in progress
 */
public interface IList {
    void add(int i);
    boolean remove(Integer i);
    boolean removeValue(int i);
    boolean removeValues(int i);
    int size();
    boolean exists();
    void print();
}
