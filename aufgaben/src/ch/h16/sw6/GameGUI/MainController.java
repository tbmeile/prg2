package ch.h16.sw6.GameGUI;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    @FXML
    private Button btnStart;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void startClick() {
        System.out.println("selected");
    }

    @FXML
    public void close() {
        System.exit(0);
    }
}
