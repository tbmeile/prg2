package ch.h16.sw5.oop5_dat5.auf2;

import javax.swing.*;
import java.awt.*;

/**
 * Created by alkazua on 03.04.16.
 * Project: prg2
 * Description: in progress
 */
public class ClickCount extends JFrame {
    public int clicks = 0;

    public ClickCount(String s) throws HeadlessException {
        super(s);
        initUi();
    }

    private void initUi() {
        setSize(200,100);

        JLabel counts = new JLabel("clicked: "+ clicks);

        JButton clicker = new JButton("Click me!");
        clicker.addActionListener(e -> {
            updateClicks(counts);
        });

        setLayout(new GridLayout());

        add(clicker);
        add(counts);

        setVisible(true);
    }

    private void updateClicks(JLabel l) {
        l.setText("clicked: "+ ++clicks);
    }

    public static void main(String[] args) {
        ClickCount cc = new ClickCount("Not Headless");
    }
}
