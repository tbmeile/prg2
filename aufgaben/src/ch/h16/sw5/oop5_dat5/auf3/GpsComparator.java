package ch.h16.sw5.oop5_dat5.auf3;

import java.util.Comparator;

/**
 * Created by alkazua on 03.04.16.
 * Project: prg2
 * Description: in progress
 */
public class GpsComparator implements Comparator<Mountain> {

    @Override
    public int compare(Mountain b1, Mountain b2) {
        // hier steht Ihre Implementation
        return Double.compare(b1.getLocation().getL(), b2.getLocation().getL());
    }

    @Override
    public boolean equals(Object other) {
        if(other == null) return false;
        return (this.getClass() == other.getClass());
    }
}
