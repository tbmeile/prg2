/* Copyright 2015 Hochschule Luzern - Technik & Architektur */
package ch.h16.sw5.oop5_dat5.auf3;

import java.util.ArrayList;
import java.util.List;

/**
 * Check natural and special order of mountains.
 * @author Peter Sollberger (peter.sollberger@hslu.ch)
 */
public final class MountainSort {
    
    /**
     * Demo.
     * @param args  not used.
     */
    public static void main(String[] args) {
        final List<Mountain> mountains = new ArrayList<>();

        System.out.println(("naime").hashCode());

        mountains.add(new Mountain("Weisshorn", 2653, new Gps(20.11222, 14.22222))); // Arosa
        mountains.add(new Mountain("Weisshorn", 4505, new Gps(55.31222, 14.22222))); // Wallis
        mountains.add(new Mountain("Pilatus", 2128, new Gps(21.55554, 14.22222)));
        mountains.add(new Mountain("Rigi", 1797, new Gps(111.11222, 14.22222)));
        mountains.add(new Mountain("Stanserhorn", 1898, new Gps(20.11222, 677.22222)));
        mountains.add(new Mountain("Titlis", 3238, new Gps(20.11222, 47.22222)));
        mountains.add(new Mountain("Bürgenstock", 1128, new Gps(210.11222, 134.22222)));
        System.out.println("\nEingabereihenfolge: \n " + mountains);

        mountains.sort(null);
        System.out.println("\nAlfabetisch nach Namen\n " + mountains);

        mountains.sort(new HeightComparator());
        System.out.println("\nnach aufsteigender Höhe: \n " + mountains);

        mountains.sort(new GpsComparator());
        System.out.println("\nnach aufsteigender Location: \n " + mountains);
    }
}
