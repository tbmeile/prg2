/* Copyright 2015 Hochschule Luzern - Technik & Architektur */
package ch.h16.sw5.oop5_dat5.auf3;

import java.util.Objects;

/**
 * 
 * @author Peter Sollberger (peter.sollberger@hslu.ch)
 */
public final class Mountain implements Comparable<Mountain> {
    private final Gps location;
    private final String name;
    private final int height;

    public Mountain(String name, int height, Gps location) {
        this.name = name;
        this.height = height;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public int getHeight() {
        return height;
    }

    public Gps getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return name + " " + height + " " + location.toString();
    }

    @Override
    public int compareTo(Mountain other) {
        return name.compareTo(other.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name)*17+height;
    }

    @Override
    public boolean equals(Object other) {
        if(other == null) return false;
        if (this.getClass() != other.getClass()) return false;

        return name.equals(((Mountain) other).getName()) &&
                height == ((Mountain) other).getHeight();
    }
}
