package ch.h16.sw5.oop5_dat5.auf3;

import java.math.BigDecimal;

/**
 * Created by alkazua on 03.04.16.
 * Project: prg2
 * Description: in progress
 */
public class Gps {
    private double b;
    private double l;

    public Gps(double b, double l) {
        this.b = b;
        this.l = l;
    }

    public double getB() {
        return b;
    }

    public double getL() {
        return l;
    }

    @Override
    public String toString() {
        return l + " " + b;
    }
}
