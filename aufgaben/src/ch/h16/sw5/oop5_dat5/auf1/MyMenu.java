package ch.h16.sw5.oop5_dat5.auf1;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.*;

/**
 * Created by alkazua on 03.04.16.
 * Project: prg2
 * Description: in progress
 */
public class MyMenu extends JFrame {
    public static MyMenu menu1;

    public static void main(String[] args) {
        menu1 = new MyMenu();
    }

    public MyMenu() {
        super("MY MENU");
        initGui();
    }

    private void initGui() {
        setSize(new Dimension(200, 100));

        // Create bar
        JMenuBar myMenuBar = new JMenuBar();

        // Create menu
        JMenu mFile = new JMenu("File");
        JMenu mHelp = new JMenu("Help");

        // Help dialog with lamda expression
        mHelp.addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(MenuEvent menuEvent) {
                //default title and icon
                JOptionPane.showMessageDialog(menu1,
                        "Fachhochschule Luzern");
            }

            @Override
            public void menuDeselected(MenuEvent menuEvent) {

            }

            @Override
            public void menuCanceled(MenuEvent menuEvent) {

            }
        });

        // Create MenuItems
        JMenuItem iNew = new JMenuItem("New");
        JMenuItem iOpen = new JMenuItem("Open");
        JMenuItem iExit = new JMenuItem("Exit");

        // Exit JFrame
        iExit.addActionListener(e -> System.exit(0));

        // Add menu items to File
        mFile.add(iNew);
        mFile.add(iOpen);
        mFile.add(iExit);

        // Add menu to bar
        myMenuBar.add(mFile);
        myMenuBar.add(mHelp);

        // add bar to JFrame
        add(myMenuBar);

        // Show JFrame
        setVisible(true);
    }
}
