package ch.h16.sw4.oop4_dat4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by alkazua on 18.03.16.
 * Project: prg2
 * Description: in progress
 */
public class ButtonUI extends JPanel implements ActionListener {
    private String[] text = {   "7", "8", "9", "+",
                                "4", "5", "6", "-",
                                "1", "2", "3", "*",
                                "0", "S", "=", "+",
                                "C"};
    private JButton[] buttons = new JButton[text.length];
    private JTextArea out = new JTextArea(1, 15);

    public ButtonUI() {
        initUI();
    }

    private void initUI() {
        // Create rows
        JPanel[] row = new JPanel[6];
        row[0] = new JPanel(new FlowLayout(FlowLayout.CENTER));
        for (int i = 1; i < row.length; i++) {
            row[i] = new JPanel(new FlowLayout(FlowLayout.CENTER));
        }

        // Create textfield
        out.setFont(new Font("Times New Roman", Font.BOLD, 14));
        out.setEditable(false);
        row[0].add(out);

        // Create buttons
        int rowIndex = 1;
        for (int i = 0; i < buttons.length; i++) {
            if (i % 4 == 0 && i != 0) rowIndex++;
            buttons[i] = new JButton(text[i]);
            buttons[i].addActionListener(this);
            row[rowIndex].add(buttons[i]);
        }

        // Append all panels as rows
        for (JPanel p : row) {
            add(p);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == buttons[0]) {
            out.append(text[0]);
        }
        if(e.getSource() == buttons[1]) {
            out.append(text[1]);
        }
        if(e.getSource() == buttons[2]) {
            out.append(text[2]);
        }
        if(e.getSource() == buttons[3]) {
            out.append(text[3]);
        }
        if(e.getSource() == buttons[4]) {
            out.append(text[4]);
        }
        if(e.getSource() == buttons[5]) {
            out.append(text[5]);
        }
        if(e.getSource() == buttons[6]) {
            out.append(text[6]);
        }
        if(e.getSource() == buttons[7]) {
            out.append(text[7]);
        }
        if(e.getSource() == buttons[8]) {
            out.append(text[8]);
        }
        if(e.getSource() == buttons[9]) {
            out.append(text[9]);
        }
        if(e.getSource() == buttons[10]) {
            out.append(text[10]);
        }
        if(e.getSource() == buttons[11]) {
            out.append(text[11]);
        }
        if(e.getSource() == buttons[12]) {
            out.append(text[12]);
        }
        if(e.getSource() == buttons[13]) {
            out.append(text[13]);
        }
        if(e.getSource() == buttons[14]) {
            out.append(text[14]);
        }
        if(e.getSource() == buttons[15]) {
            out.append(text[15]);
        }
        if(e.getSource() == buttons[16]) {
            out.setText("");
        }
    }
}
