package ch.h16.sw4.oop4_dat4;

import javax.swing.*;
import java.awt.*;

/**
 * Created by alkazua on 18.03.16.
 * Project: prg2
 * Description: in progress
 */
public class Calculator extends JFrame {

    public Calculator() {
        super("Calculator");
        setDesign();
        initGUI();
    }

    public final void setDesign() {
        try {
            UIManager.setLookAndFeel(
                "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"
            );
        } catch(Exception e) {
        }
    }

    private void initGUI() {
        setBackground(Color.black);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(new Dimension(220, 300));
        add(new ButtonUI());
        setResizable(false);
        setVisible(true);
    }

    public static void main(String[] args) {
        Calculator c = new Calculator();
    }
}
