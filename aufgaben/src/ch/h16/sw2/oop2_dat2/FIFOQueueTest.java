package ch.h16.sw2.oop2_dat2;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by alkazua on 03.03.16.
 * Project: prg2
 * Description: in progress
 */
public class FIFOQueueTest extends TestCase {
    public void testString() throws Exception {
        FIFOQueue<String> queue = new FIFOQueue<String>(5);

        String e1 = "element1";
        String e2 = "element2";
        String e3 = "element3";
        String e4 = "element4";
        String e5 = "element5";

        Assert.assertEquals(true, queue.isEmpty());
        Assert.assertEquals(null, queue.dequeue());

        queue.enqueue(e1);

        Assert.assertEquals(false, queue.isEmpty());
        Assert.assertEquals(1, queue.getNbrElements());
        Assert.assertEquals(0, queue.getHead());
        Assert.assertEquals(1, queue.getTail());

        queue.enqueue(e2);
        queue.enqueue(e3);
        queue.enqueue(e4);
        queue.enqueue(e5);

        Assert.assertEquals(true, queue.isFull());
        Assert.assertEquals(5, queue.getNbrElements());
        Assert.assertEquals(0, queue.getHead());
        Assert.assertEquals(0, queue.getTail());
        queue.enqueue(e1);

        Assert.assertEquals(e1, queue.dequeue());
        Assert.assertEquals(false, queue.isFull());
        Assert.assertEquals(4, queue.getNbrElements());

        queue.enqueue(e1);

        Assert.assertEquals(true, queue.isFull());
        Assert.assertEquals(5, queue.getNbrElements());
        Assert.assertEquals(1, queue.getHead());
        Assert.assertEquals(1, queue.getTail());
    }
}