package ch.h16.sw2.oop2_dat2;

/**
 * Created by alkazua on 04.03.16.
 * Project: prg2
 * Description: in progress
 */
public class Node<T> {
    protected T data;
    protected Node next;
    protected Node prev;

    public Node(T data) {
        this.data = data;
        this.next = null;
        this.prev = null;
    }
}
