package ch.h16.sw2.oop2_dat2;

import java.util.ArrayList;

/**
 * Created by alkazua on 03.03.16.
 * Project: prg2
 * Description: in progress
 */
public class LIFOStack<T> {
    private ArrayList<T> stack;
    private final int size;
    private int top = 0;

    public LIFOStack(int size) {
        this.size = size;
        this.stack = new ArrayList<T>();
    }

    public boolean isEmpty() {
        return (top == 0);
    }

    public boolean isFull() {
        return (top == size);
    }

    public void push(T o) {
        stack.add(o);
        top++;
    }

    public T pop() {
        top--;
        T t = stack.get(top);
        stack.remove(top);
        return t;
    }
}
