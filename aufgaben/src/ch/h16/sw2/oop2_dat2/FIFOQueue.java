package ch.h16.sw2.oop2_dat2;

import java.math.BigInteger;

/**
 * Created by alkazua on 03.03.16.
 * Project: prg2
 * Description: in progress
 */
public class FIFOQueue<T> {
    private T[] queue;
    private final int size;
    private int head, tail, nbrElements = 0;

    @SuppressWarnings("unchecked")
    public FIFOQueue(int size) {
        this.size = size;
        this.queue = (T[]) new Object[size];
    }

    public void enqueue(T o) {
        if (!isFull()) {
            queue[tail] = o;
            nbrElements++;
            if(tail < size-1) tail++;
            else tail = 0;
        } else {
            System.out.println("Queue full..");
        }
    }

    public T dequeue() {
        T t = null;
        if (!isEmpty()) {
            t = queue[head];
            nbrElements--;
            if(head < size-1) head++;
            else head = 0;
        } else {
            System.out.println("Queue empty..");
        }
        return t;
    }

    public boolean isEmpty() {
        return (nbrElements == 0);
    }

    public boolean isFull() {
        return (nbrElements == size);
    }

    public int getNbrElements() {
        return nbrElements;
    }

    public int getHead() {
        return head;
    }

    public int getTail() {
        return tail;
    }
}