package ch.h16.sw2.oop2_dat2;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by alkazua on 03.03.16.
 * Project: prg2
 * Description: in progress
 */
public class LIFOStackTest extends TestCase {
    public void testStackString() throws Exception {
        LIFOStack<String> stack = new LIFOStack<String>(5);

        Assert.assertEquals(true, stack.isEmpty());

        String e1 = "element1";
        String e2 = "element2";
        String e3 = "element3";
        String e4 = "element4";
        String e5 = "element5";

        stack.push(e1);

        Assert.assertEquals(false, stack.isEmpty());

        stack.push(e2);
        stack.push(e3);
        stack.push(e4);

        Assert.assertEquals(false, stack.isFull());

        stack.push(e5);

        Assert.assertEquals(true, stack.isFull());

        Assert.assertEquals("element5", stack.pop());
        Assert.assertEquals(false, stack.isFull());

        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();

        Assert.assertEquals(true, stack.isEmpty());
    }
}