package ch.h16.sw2.oop2_dat2;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by alkazua on 04.03.16.
 * Project: prg2
 * Description: in progress
 */
public class DoubleLinkedListTest extends TestCase {
    public void testDLLString() throws Exception {
        DoubleLinkedList<String> list = new DoubleLinkedList<String>();

        String e1 = "element1";
        String e2 = "element2";
        String e3 = "element3";
        String e4 = "element4";
        String e5 = "element5";

        list.addFirst(e1);
        Assert.assertEquals(true, list.isFound(e1));
        list.addFirst(e2);
        Assert.assertEquals(true, list.isFound(e1));
        Assert.assertEquals(true, list.isFound(e2));
        System.out.println(list.toString());
        list.addLast(e3);
        System.out.println(list.toString());
        list.addLast(e4);
        list.addFirst(e5);
        System.out.println(list.toString());
    }
}