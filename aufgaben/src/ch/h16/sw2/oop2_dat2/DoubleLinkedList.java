package ch.h16.sw2.oop2_dat2;

/**
 * Created by alkazua on 04.03.16.
 * Project: prg2
 * Description: in progress
 */
public class DoubleLinkedList<T> {
    private Node<T> head;
    private Node<T> tail;

    public DoubleLinkedList() {
        head = tail = null;
    }

    public void addFirst(T data) {
        Node<T> newNode = new Node<T>(data);

        if(isEmpty()) {
            head = tail = newNode;
        } else {
            newNode.next = head;
            head.prev = newNode;
            head = newNode;
        }
    }

    public void addLast(T data) {
        final Node<T> newNode = new Node<T>(data);

        if(isEmpty()) {
            head = tail = newNode;
        } else {
            newNode.prev = tail;
            tail.next = newNode;
            tail = newNode;
        }
    }

    public boolean isEmpty() {
        return (head == null);
    }

    public boolean isFull() {
        return false;
    }

    public boolean isFound(T data) {
        Node<T> search = head;
        while((search != null) && (!search.data.equals(data))) {
            search = search.next;
        }
        return (search.data.equals(data));
    }

    @Override
    public String toString() {
        Node<T> currentNode = head;
        String list = ("[head]--[");
        while (currentNode != null) {
            list += currentNode.data+"]--[";
            currentNode = currentNode.next;
        }
        return list+"tail]";
    }
}