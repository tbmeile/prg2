package ch.h16.sw2.snake.model;

import java.awt.*;
import java.util.LinkedList;

/**
 * Created by alkazua on 07.03.16.
 * Project: prg2
 * Description: in progress
 */
public class Snake extends LinkedList<Point> {
    private Direction direction, oldDir;

    public Snake(Point p) {
        addFirst(p);
    }

    // Increase snake's size
    public void grow(int n) {
        while(n > 0) {
            add(new Point(getLast()));
            n--;
        }
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    // Reverse the whole snake
    public void reverse() {
        Point temp = new Point();
        for (int p = size()-1; p > (size()-1)/2; p--) {
            if (get(size() - p) != get(p)) {
                // swap with counter part
                temp.setLocation(get(size()-1 - p));
                get(size()-1 - p).setLocation(get(p));
                get(p).setLocation(temp);
            }
        }
    }

    // Check if head hits body
    public boolean detectCollision() {
        boolean isCollision = false;
        if (size() > 2) {
            for(int i = 0; i < size()-1; i++) {
                if (getFirst().getLocation().equals(get(i+1).getLocation())) isCollision = false; // temp disabled
            }
        }
        return isCollision;
    }

    // Act according to Direction
    public void act(int size) {
        if (getDirection() == Direction.RIGHT) {
            //if(oldDir == Direction.LEFT) reverse();
            move(size, 0);
        }
        if(getDirection() == Direction.LEFT) {
            //if(oldDir == Direction.RIGHT) reverse();
            move(-size, 0);

        }
        if(getDirection() == Direction.UP) {
            //if(oldDir == Direction.DOWN) reverse();
            move(0, -size);
        }
        if(getDirection() == Direction.DOWN) {
            //if(oldDir == Direction.UP) reverse();
            move(0, +size);
        }

        oldDir = getDirection();
    }

    // Direction all snake nodes
    private void move(int x, int y) {
        for(int n = size()-1; n >= 1; n--) {
            get(n).setLocation(get(n-1));
        }
        getFirst().x += x;
        getFirst().y += y;
    }

    // Print out snake
    @Override
    public String toString() {
        String snake = "########\n";
        for (Point part : this) {
            snake += "x: " + part.getX() + "\ny: " + part.getY() +"\n"+"--------\n";
        }
        return snake;
    }
}