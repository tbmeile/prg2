package ch.h16.sw2.snake.model;

import java.awt.*;
import java.util.Random;

/**
 * Created by alkazua on 07.03.16.
 * Project: prg2
 * Description: in progress
 */
public class RndPoint extends Point {
    private int size = 10;

    public RndPoint() {
        Random generator = new Random();
        int x = generator.nextInt(50),
            y= generator.nextInt(50);

        setLocation(x*size, y*size);
    }
}
