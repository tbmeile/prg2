package ch.h16.sw2.snake.model;

/**
 * Created by alkazua on 06.03.16.
 * Project: prg2
 * Description: in progress
 */
public enum Direction {
    LEFT, RIGHT, UP, DOWN;
}
