package ch.h16.sw2.snake.model;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by alkazua on 06.03.16.
 * Project: prg2
 * Description: in progress
 */
public class Boss extends Point implements ActionListener {
    private boolean alive;
    private Timer time;

    public Boss() {
        kill();
        time = new Timer(5000, this);
        time.start();
    }

    public boolean isAlive() {
        return alive;
    }

    public void kill() {
        setLocation(1000, 1000);
        alive = false;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        setLocation(new RndPoint());
        alive = true;
    }
}
