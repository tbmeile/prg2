package ch.h16.sw2.snake.test;

import ch.h16.sw2.snake.model.Direction;
import ch.h16.sw2.snake.model.Snake;
import junit.framework.Assert;
import junit.framework.TestCase;

import java.awt.*;

/**
 * Created by alkazua on 07.03.16.
 * Project: prg2
 * Description: in progress
 */
public class SnakeTest extends TestCase {
    public void testSnake() throws Exception {
        Snake snake = new Snake(new Point());
        Assert.assertEquals(false, snake.isEmpty());
        Assert.assertEquals(1, snake.size());

        // Check growth
        snake.grow(5);
        Assert.assertEquals(6, snake.size());

        // Check movements UP/DOWN
        Point start = snake.getFirst().getLocation();
        snake.setDirection(Direction.RIGHT);
        snake.act(10);
        Assert.assertEquals(start.getX()+10, snake.getFirst().getLocation().getX());
        Assert.assertEquals(start.getY(), snake.getFirst().getLocation().getY());
        snake.setDirection(Direction.LEFT);
        snake.act(10);

        // Check movements LEFT/RIGHT
        start = snake.getFirst().getLocation();
        snake.setDirection(Direction.DOWN);
        snake.act(10);
        Assert.assertEquals(start.getX(), snake.getFirst().getLocation().getX());
        Assert.assertEquals(start.getY()+10, snake.getFirst().getLocation().getY());
        snake.setDirection(Direction.UP);
        snake.act(10);

        // Check reverse
        Point last = snake.getLast().getLocation();;
        Point first = snake.getFirst().getLocation();
        snake.reverse();
        Assert.assertEquals(first, snake.getLast());
        Assert.assertEquals(last, snake.getFirst());
    }
}