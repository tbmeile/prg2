package ch.h16.sw2.snake.view;

import ch.h16.sw2.snake.model.RndPoint;
import ch.h16.sw2.snake.model.Boss;
import ch.h16.sw2.snake.model.Direction;
import ch.h16.sw2.snake.model.Food;
import ch.h16.sw2.snake.model.Snake;

import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by alkazua on 05.03.16.
 * Project: prg2
 * Description: in progress
 */
public final class SnakeGame extends Applet implements KeyListener, ActionListener {
    private int width = 500, height = 500;
    private int size = 10;
    private Timer time;
    private Graphics bufferGraphics;
    private Image offscreen;
    private int points = 0;
    private Snake snake;
    private Food food;
    private Boss boss;

    public SnakeGame() {
        time = new Timer(1000, this);
        time.start();
    }

    @Override
    public void init() {
        setBackground(new Color(111, 37, 111));
        setSize(width, height);
        offscreen = createImage(getWidth(),getHeight());
        bufferGraphics = offscreen.getGraphics();
        setFocusable(true);
        addKeyListener(this);
        startGame();
    }

    private void startGame() {
        setSpeed(100);
        snake = new Snake(new RndPoint());
        //snake.grow(10);
        food = new Food(new RndPoint());
        boss = new Boss();
        points = 0;
    }

    @Override
    public void paint(Graphics g) {
        isDoubleBuffered();
        bufferGraphics.clearRect(0,0,getWidth(),getHeight());
        drawScore();
        drawSnake();
        drawFood();
        drawBoss();
        g.drawImage(offscreen,0,0,this);
    }

    @Override
    public void update(Graphics g) {
        paint(g);
    }

    private void drawScore() {
        bufferGraphics.setColor(new Color(0xA66FA6));
        bufferGraphics.setFont(new Font("Verdana", Font.BOLD, 12));
        bufferGraphics.drawString("Points: " + points, 0, 10);
    }

    private void drawSnake () {
        Color head = new Color(0x370037);
        for (Point p : snake) {
            bufferGraphics.setColor(head);
            bufferGraphics.fillRect(p.x, p.y, size-1, size-1);
            repaint();
        }
    }

    private void drawFood () {
        bufferGraphics.setColor(new Color(0xA66FA6));
        bufferGraphics.fillRect(food.x, food.y, size-1, size-1);
    }

    private void drawBoss () {
        if(boss.isAlive()) {
            bufferGraphics.setColor(new Color(0xA66FA6));
            bufferGraphics.fillOval(boss.x, boss.y, size*2, size*2);
        }
    }

    private void checkingWalls() {
        if(snake.getFirst().x <= -size ||
           snake.getFirst().x >= getWidth() ||
           snake.getFirst().y <= -size ||
           snake.getFirst().y >= getHeight())
        {
            init();
        }
    }

    private void checkingFood() {
        if (snake.getFirst().x == food.x &&
            snake.getFirst().y == food.y)
        {
            food.setLocation(new RndPoint());
            snake.grow(1);
            setSpeed(time.getDelay()-1);
            points++;
        }
    }

    private void checkingBoss() {
        if (snake.getFirst().x >= boss.x &&
            snake.getFirst().x <= boss.x + size &&
            snake.getFirst().y >= boss.y &&
            snake.getFirst().y <= boss.y + size)
        {
            boss.kill();
            snake.grow(4);
            setSpeed(time.getDelay()-4);
            points+=4;
        }
    }

    private void setSpeed(int s) {
        if (time.getDelay() > 50) {
            time.stop();
            time.setDelay(s);
            time.start();
        }
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {}

    @Override
    public void keyPressed(KeyEvent keyEvent) {}

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        String txt=KeyEvent.getKeyText(keyEvent.getKeyCode());
        if(txt.equalsIgnoreCase("Right"))
            snake.setDirection(Direction.RIGHT);
        if(txt.equalsIgnoreCase("Left"))
            snake.setDirection(Direction.LEFT);
        if(txt.equalsIgnoreCase("Up"))
            snake.setDirection(Direction.UP);
        if(txt.equalsIgnoreCase("Down"))
            snake.setDirection(Direction.DOWN);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        snake.act(size);

        checkingWalls();
        checkingFood();
        checkingBoss();
        if (snake.detectCollision()) {
            init();
        };
    }
}