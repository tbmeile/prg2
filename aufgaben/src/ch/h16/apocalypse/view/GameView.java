package ch.h16.apocalypse.view;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by alkazua on 11.03.16.
 * Project: prg2
 * Description: in progress
 */
public class GameView extends JApplet implements KeyListener {
    private Graphics bufferGraphics;
    private Image offscreen;

    @Override
    public void init() {
        setBackground(Color.black);
        setFocusable(true);
        addKeyListener(this);
        addSettings();
        offscreen = createImage(getWidth(),getHeight());
        bufferGraphics = offscreen.getGraphics();
    }

    private void addSettings() {
        JButton test = new JButton();

    }

    @Override
    public void paint(Graphics g) {
        isDoubleBuffered();
        bufferGraphics.clearRect(0,0,getWidth(),getHeight());
        drawLogo();
        drawTitle();
        g.drawImage(offscreen,0,0,this);
    }

    private void drawTitle() {
        String title = getParameter("Title");
        bufferGraphics.setFont(new Font("Impact", Font.PLAIN, 42));
        bufferGraphics.setColor(Color.green);
        bufferGraphics.drawString(title, getWidth()/2, 42);
    }

    private void drawLogo() {
        try {
            final BufferedImage logo = ImageIO.read(
                    new File("/home/alkazua/Code/prg2/aufgaben/src/ch/h16/apocalypse/img/one_punch.jpg")
            );
            bufferGraphics.drawImage(logo, getWidth()-logo.getWidth(), getHeight()-logo.getHeight(), this);
        } catch (IOException e) {
            System.out.println("File not found");
        }
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {

    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }
}