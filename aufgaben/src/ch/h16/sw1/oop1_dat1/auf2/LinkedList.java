package ch.h16.sw1.oop1_dat1.auf2;

import ch.h16.sw1.oop1_dat1.auf1.Konto;

/**
 * Created by alkazua on 26.02.16.
 * Project: prg2
 * Description: in progress
 */
public class LinkedList {
    private ListNode head;

    public LinkedList() {
    }

    public static void main(String[] args) {
        LinkedList ll = new LinkedList();

        ll.insert(new Konto(20000, 1.2));
        ll.insert(new Konto(18000, 1.2));

        System.out.println(ll.toString());
    }

    public void insert(Konto konto) {
        if (!isFound(konto)) {
            head = new ListNode(konto, head);
        }
    }

    public void remove(Konto konto) {
        ListNode currentNode = head;
        ListNode prevNode = null;
        while ((currentNode != null) && !konto.equals(currentNode.getKonto())) {
            prevNode = currentNode;
            currentNode = prevNode.getNext();
        }

        // clean up list
        if (currentNode != null) {
            if (currentNode == head) {
                head = currentNode.getNext();
            }
            else {
                prevNode.setNext(currentNode.getNext());
            }
        }
    }

    public boolean isFound(Konto konto) {
        ListNode currentNode = head;
        while ((currentNode != null) && !konto.equals(currentNode.getKonto())) {
            currentNode = currentNode.getNext();
        }
        return (currentNode != null);
    }

    @Override
    public String toString() {
        ListNode currentNode = head;
        String list = ("[head]--[");
        while (currentNode != null) {
            list += currentNode.getKonto().getSaldo()+"]--[";
            currentNode = currentNode.getNext();
        }
        return list+"/]";
    }
}