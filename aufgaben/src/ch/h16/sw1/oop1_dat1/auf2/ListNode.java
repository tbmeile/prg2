package ch.h16.sw1.oop1_dat1.auf2;

import ch.h16.sw1.oop1_dat1.auf1.Konto;

/**
 * Created by alkazua on 26.02.16.
 * Project: prg2
 * Description: in progress
 */
public class ListNode {
    private Konto konto;
    private ListNode next;

    public ListNode(Konto konto, ListNode next) {
        this.konto = konto;
        this.next = next;
    }

    public void setKonto(Konto konto) {
        this.konto = konto;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    public Konto getKonto() {
        return konto;
    }

    public ListNode getNext() {
        return next;
    }
}
