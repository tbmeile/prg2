package ch.h16.sw1.oop1_dat1.auf1;

import java.util.ArrayList;

/**
 * Created by alkazua on 26.02.16.
 * Project: prg2
 * Description: in progress
 */
public class Bank {
    private String name;
    private ArrayList<Konto> arrayList;

    public static void main(String[] args) {
        Bank bank1 = new Bank("Raiffeisen");

        bank1.openKonto(25000, 1.2);
        bank1.openSpar(44000, 2.3, 20000);

        bank1.printAccounts();
        bank1.printFund();
    }

    public Bank(String name) {
        this.name = name;
        arrayList = new ArrayList<Konto>();
    }

    public void openKonto(double saldo, double rate) {
        arrayList.add(new Konto(saldo, rate));
    }

    public void openSpar(double saldo, double rate, double maxOut) {
        arrayList.add(new Spar(saldo, rate, maxOut));
    }

    public int noOfAccounts() {
        return arrayList.size();
    }

    public void printAccounts() {
        System.out.println("Anzahl Kontos: " + noOfAccounts());
    }

    public void printFund() {
        double fund = 0;
        for (Konto k : arrayList) {
            fund += k.getSaldo();
        }
        System.out.println("Gesamte Mittel: " + fund);
    }
}