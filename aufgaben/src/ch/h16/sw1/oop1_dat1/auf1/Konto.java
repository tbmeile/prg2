package ch.h16.sw1.oop1_dat1.auf1;

/**
 * Created by alkazua on 26.02.16.
 * Project: prg2
 * Description: in progress
 */
public class Konto {
    private static int count;
    private int no;
    private double saldo;
    private double rate;

    public Konto(double saldo, double rate) {
        this.saldo = saldo;
        this.rate = rate;
        this.no = count++;
    }

    public Konto() {
        this(0.0, 1.2);
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public void payIn(final double amount) {
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("Sie zahlen: " + amount + " ein.");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        saldo += amount;
        print();
    }

    public void payOut(final double amount) {
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("Sie möchten: " + amount + " auszahlen.");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        saldo += amount;
    }

    public void print() {
        System.out.println("No: " + no);
        System.out.println("Saldo: " + saldo);
        System.out.println("Rate: " + rate);
    }
}
