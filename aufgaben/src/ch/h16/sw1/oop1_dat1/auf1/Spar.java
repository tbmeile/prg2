package ch.h16.sw1.oop1_dat1.auf1;

/**
 * Created by alkazua on 26.02.16.
 * Project: prg2
 * Description: in progress
 */
public class Spar extends Konto{
    private double maxOut;

    public Spar(double saldo, double rate, double maxOut) {
        super(saldo, rate);
        this.maxOut = maxOut;
    }

    public Spar() {
        this(0, 2.3, 20000);
    }

    public static void main(String[] args) {
        Spar myKonto = new Spar(500000, 2.3, 20000);

        myKonto.print();

        myKonto.payOut(25000);
        myKonto.payOut(20000);
        myKonto.payIn(5000);
    }

    @Override
    public double getSaldo() {
        return super.getSaldo();
    }

    @Override
    public void payOut(final double amount) {
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println("Sie möchten: " + amount + " auszahlen.");
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        if (amount <= maxOut) {
            super.setSaldo(getSaldo() - amount);
            print();
        } else {
            System.out.println("Maximale Auszahlung überschritten um: " + (amount-maxOut));
        }
    }

    @Override
    public void print() {
        super.print();
        System.out.println("Maximale Auszahlung: " + maxOut);
    }
}