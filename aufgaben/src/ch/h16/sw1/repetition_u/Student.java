package ch.h16.sw1.repetition_u;

/**
 * Created by alkazua on 26.02.16.
 * Project: prg2
 * Description: in progress
 */
public class Student extends Person {
    private static int number = 1000;
    private int studentNr;
    private String course;

    public Student(final String name, final String firstName) {
        super(name, firstName);
        setStudentNr();
    }

    /**
     * Erstellt einen Studenten
     * @param name          Name
     * @param firstName     Vorname
     * @param course        Kurs
     */
    public Student(String name, String firstName, String course) {
        super(name, firstName);
        this.course = course;
        setStudentNr();
    }

    public void setStudentNr() {
        studentNr = number++;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public int getStudentNr() {
        return studentNr;
    }

    public String getCourse() {
        return course;
    }

    @Override
    public void print() {
        System.out.println("Id: " + studentNr);
        System.out.println("Kurs: " + course);
        super.print();
    }
}