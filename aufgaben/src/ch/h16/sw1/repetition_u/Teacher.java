package ch.h16.sw1.repetition_u;

/**
 * Created by alkazua on 26.02.16.
 * Project: prg2
 * Description: in progress
 */
public class Teacher extends Person {
    private int salary;
    private String subject;

    /*
     Konstruktor, der den Namen und den Vornamen erhält
 Konstruktor, der den Namen, den Vornamen, das Fach und die Honorar-Gruppe erhält
 getter und setter Methoden für die Attribute (soweit sinnvoll)
 print, um die Attribute auszugeben
     */

    public Teacher(String name, String firstName) {
        super(name, firstName);
    }

    public Teacher(String name, String firstName, int salary, String subject) {
        super(name, firstName);
        this.salary = salary;
        this.subject = subject;
    }

    public int getSalary() {
        return salary;
    }

    public String getSubject() {
        return subject;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public void print() {
        super.print();
        System.out.println("Fach: " + subject + " " +
                           "Salary: " + salary);
    }
}