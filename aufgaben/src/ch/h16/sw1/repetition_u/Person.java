package ch.h16.sw1.repetition_u;

/**
 * Created by alkazua on 26.02.16.
 * Project: prg2
 * Description: in progress
 */
public class Person {
    private String name;
    private String firstName;

    public Person(final String name, final String firstName) {
        this.name = name;
        this.firstName = firstName;
    }

    public Person() {
        this("unknown", "unknown");
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getName() {
        return name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void print() {
        System.out.println("Name: " + name);
        System.out.println("Vorname: " + firstName);
    }
}