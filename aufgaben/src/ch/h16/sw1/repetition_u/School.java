package ch.h16.sw1.repetition_u;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alkazua on 26.02.16.
 * Project: prg2
 * Description: in progress
 */
public class School {
    private String name;
    private HashMap<Integer, Student> studentList;

    /*
Attribute:
 name
 studentList (HashMap, Schlüssel ist studentNumber)
Methoden:
 Konstruktor, der den Namen der Schule erhält
 getter und setter Methoden für die Attribute (soweit sinnvoll)
 enrolStudent, um einen neuen Studierenden zu immatrikulieren
 printStudents, um die Studierenden auszugeben
Klassenmethode:
 main
Mit Hilfe der Klassenmethode main() sollen ein Schule eröffnet, einige Studierende
immatrikuliert und anschliessend auch ausgegeben werden.
     */
    public static void main(String[] args) {
        School hslu = new School("Hslu");

        Student alan = new Student("Meile", "Alan", "Informatik");
        Student bravin = new Student("Test", "Bravin", "Wissenschaftler");

        hslu.enrolStudent(alan);
        hslu.enrolStudent(bravin);

        hslu.printStudents();
    }

    public School(String name) {
        this.name = name;
        this.studentList = new HashMap();
    }

    public void enrolStudent(Student student) {
        studentList.put(student.getStudentNr(), student);
    }

    public void printStudents() {
        for(Map.Entry<Integer, Student> entry : studentList.entrySet()) {
            int id = entry.getKey();
            Student student = entry.getValue();

            // Ausgabe
            student.print();
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStudentList(HashMap studentList) {
        this.studentList = studentList;
    }

    public String getName() {
        return name;
    }

    public HashMap getStudentList() {
        return studentList;
    }
}